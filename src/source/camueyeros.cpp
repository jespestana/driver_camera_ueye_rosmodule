/*
 * CamueyeCamera.cpp
 *
 *  Created on: Jan 17, 2013
 *      Author: jespestana
 */

#include "camueyeros.h"

#define DRONE_UEYE_DEFAULT_RATE 5.0*4.0

CamUeyeROS::CamUeyeROS( int hcam_ident) :
    cvgThread( (std::string("cvg_ueye_camera")+cvg_int_to_string(hcam_ident)).c_str() ),
    DroneModule(droneModule::active,DRONE_UEYE_DEFAULT_RATE),
    camera_started(false),
    got_thread_sleep_time(false),
    last_timestamp(0),
    hcam_ident_num(hcam_ident),
    myCamera( hcam_ident),
    config_filename_is_set(false),
    frameBuffer(NULL),
    frame(NULL),
    bytesperpixel(3), num_channels(3) {
    got_image = false;

    // initialize thread_sleep_time
    got_thread_sleep_time = false;
    thread_sleep_time = ( (double)1.0 ) / (2.0*5.0); // min fps about 5
    ros_frame_count = 0;
    thread_start();
}

CamUeyeROS::~CamUeyeROS() {
    stop();
    thread_stop();
}

void CamUeyeROS::set_config_filename( std::string config_filenamei ) {
    // convert to wchar_t*
    std::wstring config_file_w = std::wstring( config_filenamei.begin(), config_filenamei.end());

    // check configuration file path length
    std::size_t length_wstr = config_file_w.length();
    if ( length_wstr >= max_configuration_filepath_length ) {
        throw std::runtime_error("configuration_file path too long");
    }

    // copyt to internal config_filename variable
    config_filename[length_wstr] = '\0';
    config_file_w.copy( config_filename, length_wstr, 0);

    config_filename_is_set = true;
}

bool CamUeyeROS::start() {
    std::cout << "Entering CamUeyeROS::start()" << std::endl;

    camera_mutex.lock();
    if ( camera_started ) {
        camera_mutex.unlock();
        return true;
    }

    if ( !config_filename_is_set ) {
        std::cout << "configuration file name/path is not yet set/defined." << std::endl;
        return false;
    }

    // Load configuration file
    if ( !myCamera.startCam(config_filename) || !myCamera.startLive() ) {
        std::string exception_string("[UEYE_CVG_NODE] Configuration file for ueye_camera"); exception_string += hcam_ident_num + ", could not be loaded.";
        throw std::runtime_error( exception_string );
        std::cout << "error ocurred during camera initialization." << std::endl;
    }

    // initialize thread_sleep_time
    got_thread_sleep_time = false;
    thread_sleep_time = ( (double)1.0 ) / (2.0*5.0); // min fps about 5

    myCamera.getImageSize( &width, &height);
#ifdef CAMUEYECAMERA_DEBUG
    std::cout << "myCamera image size: width = " << width << " height = " << height << std::endl;
#endif
    if (frameBuffer != NULL) {
        delete[] frameBuffer;
        frameBuffer = NULL;
    }
    frameBuffer = new char[width*height*bytesperpixel];

    if (frame != NULL) {
        cvReleaseImageHeader(&frame);
        frame = NULL;
    }
    if (frame == NULL) {
        frame = cvCreateImageHeader(cvSize(width, height), IPL_DEPTH_8U, num_channels);
    }

    camera_started = true;
    camera_mutex.unlock();

    std::cout << "Exiting CamUeyeROS::start()" << std::endl;
    return true;
}

void CamUeyeROS::thread_run() {
    while (true) {
        camera_mutex.lock();
        if ( !camera_started ) {
            camera_mutex.unlock();
            usleep( (ulong) (thread_sleep_time * 1e6) );
            continue;
        }

        if ( getImage() ) {
            got_image = true;
            if (!got_thread_sleep_time) {
                got_thread_sleep_time = true;
                double camera_fps = myCamera.getFps();
                camera_fps = MAX( camera_fps, 5.0);
                thread_sleep_time = ( (double)1.0 ) / (2.0*camera_fps);
                std::cout << "thread_sleep_time:" << thread_sleep_time << std::endl;
                std::cout << "camera_fps:" << myCamera.getFps() << std::endl;
            }

        }
        camera_mutex.unlock();

        usleep( (ulong) (thread_sleep_time * 1e6) );
    }
}

bool CamUeyeROS::stop() {
    std::cout << "Entering CamUeyeROS::stop()" << std::endl;
    camera_mutex.lock();
    if ( !camera_started ) {
        camera_mutex.unlock();
        return true;
    }

    myCamera.stopCam();

    if (frameBuffer != NULL) {
        delete[] frameBuffer;
        frameBuffer = NULL;
    }

    if (frame != NULL) {
        cvReleaseImageHeader(&frame);
        frame = NULL;
    }

    camera_started = false;
    camera_mutex.unlock();
    std::cout << "Exiting CamUeyeROS::stop()" << std::endl;
    return true;
}

void CamUeyeROS::open(ros::NodeHandle &nIn, std::string moduleName, std::string topicName)
{
    if (!droneModuleOpened) {
        //Node
        DroneModule::open(nIn,moduleName);

        image_transport::ImageTransport it(nIn);
        if ( topicName.size() == 0 )
        {
            img_publisher         = it.advertise(moduleName+"/image_raw", 3, true);
            camera_info_publisher = nIn.advertise<sensor_msgs::CameraInfo>(moduleName+"/camera_info",1,true);

        }
        else
        {
            img_publisher         = it.advertise(topicName+"/image_raw", 3, true);
            camera_info_publisher = nIn.advertise<sensor_msgs::CameraInfo>(topicName+"/camera_info",1,true);
        }
        //Flag of module opened
        droneModuleOpened=true;
    }

    startVal();
}

void CamUeyeROS::close() {}

bool CamUeyeROS::resetValues()
{
    if(!DroneModule::resetValues())
        return false;

    // CamUeyeROS does not have a clearly defined reset behaviour for now

    return true;
}

bool CamUeyeROS::startVal() {
    if(!DroneModule::startVal())
        return false;

    return start();
}

bool CamUeyeROS::stopVal()
{
    if(!DroneModule::stopVal())
        return false;

    return stop();
}

bool CamUeyeROS::run()
{
    if(!DroneModule::run())
    {
        return false;
    }

    publishImage();
    set_moduleRate( 4.0/(thread_sleep_time) );

    return true;
}

bool CamUeyeROS::getImage() {
    // only enter this funcion with camera_mutex locked !!!
    ulong timestamp;
    bool successful = true;

    successful = myCamera.GetImage( frameBuffer, &timestamp);

    if (successful) {
        if (last_timestamp != timestamp) {
            last_timestamp = timestamp;
            successful = true;
            cvSetData(frame, frameBuffer, width * num_channels);
        } else {
            successful = false;
        }
    }
    return successful;
}

void CamUeyeROS::publishImage() {
    camera_mutex.lock();
    if ( !camera_started ) {
        camera_mutex.unlock();
        return;
    }

    if ( got_image ) {
        if ( frame != NULL ) {
            cv::Mat last_captured_image(frame);
            cv_bridge::CvImage my_cv_image;

            my_cv_image.image              = last_captured_image;
            my_cv_image.encoding           = "bgr8";
            sensor_msgs::ImagePtr img_msg  = my_cv_image.toImageMsg();
            img_msg->header.stamp          = ros_cam_info.header.stamp = ros::Time::now();
            img_msg->header.seq            = ros_cam_info.header.seq   = ros_frame_count++;
            img_msg->header.frame_id       = ros_cam_info.header.frame_id;

            img_publisher.publish( img_msg );
            camera_info_publisher.publish(ros_cam_info);
        }
        got_image = false;
    }
    camera_mutex.unlock();
}

void CamUeyeROS::loadIntrinsicsFile(std::string cam_intr_filename, std::string camera_name) {

  if (camera_calibration_parsers::readCalibration(cam_intr_filename, camera_name , ros_cam_info)) {
     ROS_DEBUG_STREAM("Loaded intrinsics parameters for UEye camera " << camera_name);
  }

}
